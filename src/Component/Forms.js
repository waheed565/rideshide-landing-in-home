import React, {useEffect, useState} from "react";
import {useForm, useFormContext} from "react-hook-form";
import {Input, Select} from "@chakra-ui/react";
import {Form} from "semantic-ui-react";

import axios from "axios";
import {URI} from "../constant";

export const FormOne = ({formContent}) => {
    const methods = useFormContext();
    const {reset, register} = methods;
    const {handleSubmit, errors} = useForm();
    const [error, setError] = useState({})
    useEffect(() => {
        reset({...formContent.one}, {errors: true});
    }, []);

    return (
        <form className="p-4">
            <div className="row">
                <div className="col-md-12 mt-4">
                    <Form.Field>
                        <div className="ui left icon w-100 input">
                            <input className="shadow-sm form-control w-100" type="text"
                                   ref={register({required: true})}
                                   placeholder="Enter Your Name" name="name"/>
                            <i className="user icon"></i>
                        </div>
                    </Form.Field>
                    <div className='text-left'>
                        {errors.name && errors.name.type === "required" &&
                        <span style={{color: "red"}}>Name is required</span>}
                        {error.name && <span className="Errors">
                                                      {error.name[0]}
                                                        </span>}
                    </div>
                </div>
                <div className="col-md-12 mt-4">
                    <Form.Field>
                        <div className="ui left icon w-100 input">
                            <input className="shadow-sm form-control w-100" type="text"
                                   ref={register({required: true})}
                                   placeholder="Enter Your Number No" name="phone"/>
                            <i className="phone icon"></i>
                        </div>
                    </Form.Field>
                    <div className='text-left'>
                        {errors.phone && errors.phone.type === "required" &&
                        <span style={{color: "red"}}>Phone is required</span>}
                        {error.phone && <span className="Errors">
                                                  {error.phone[0]}
                                                    </span>}
                    </div>
                </div>
                <div className="col-md-12 mt-4">
                    <Form.Field>
                        <div className="ui w-100 left icon input">
                            <input className="shadow-sm  form-control w-100" type="text"
                                   ref={register({required: true})}
                                   placeholder="Enter Your Email" name="email"/>
                            <i className="mail icon"></i>
                        </div>
                    </Form.Field>
                    <div className='text-left'>
                        {errors.email && errors.email.type === "required" &&
                        <span style={{color: "red"}}>Email is required</span>}
                        {error.email && <span className="Errors">
                                                  {error.email[0]}
                                                    </span>}
                    </div>
                </div>
                <div className="col-md-12 mt-4">
                    <Form.Field>
                        <div className="ui w-100 left icon input">
                            <input className="form-control shadow w-100" type="text"
                                   ref={register({required: true})}
                                   placeholder="Enter Your CNIC" name="cnic"/>
                            <i className="id card icon"></i>
                        </div>
                    </Form.Field>
                    <div className='text-left'>
                        {errors.cnic && errors.cnic.type === "required" &&
                        <span style={{color: "red"}}>CNIC is required</span>}
                        {error.cnic && <span className="Errors">
                                                  {error.cnic[0]}
                                                    </span>}
                    </div>
                </div>
            </div>
        </form>
    );
};


export const TwoForms = ({formContent}) => {
    const methods = useFormContext();
    const {reset, register} = methods;
    const {handleSubmit, errors} = useForm();
    const [error, setError] = useState({})
    const [color, setColor] = useState([]);
    useEffect(() => {
        const result = axios.get(URI + "getColor")
            .then(response => {
                console.log("ses", response.data.getColor);
                setColor(response.data.getColor);
            }).catch(error => {
                console.log(error);
            })
    }, [])
    useEffect(() => {
        reset({...formContent.three}, {errors: true});
    }, []);

    return (
       <>

       </>
    );
};
