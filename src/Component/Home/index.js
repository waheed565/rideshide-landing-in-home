import React, {useState} from "react";
import Img from '../../assist/images/logo/ride-shide.png';
import './style.css';
import { useHistory } from "react-router-dom";
import {useForm} from "react-hook-form";
import {Form} from 'semantic-ui-react'
import axios from "axios";
import {toast} from 'react-toastify';

import {URI} from "../../constant";
import {Tab, TabList, TabPanel, TabPanels, Tabs} from "@chakra-ui/react";
import CarForm from "../Form/index";
import {Link} from "react-router-dom";

toast.configure()
const Home = () => {
    let history = useHistory();
    const {register, handleSubmit,formState: { errors }} = useForm();
    const [error, setError] = useState({})
    const onSubmit = data => {

        axios.post(URI + 'addDriverInfo', {
            name: data.name,
            email: data.email,
            phone: data.phone,
            cnic: data.cnic,
        })
            .then(response => {
history.push("/form",{
                    name: data.name,
                    email: data.email,
                    phone: data.phone,
                    cnic: data.cnic,

                });
                console.log(response);
                // toast.success('Data submitted successfully', {autoClose: 3000})
                // window.location.href = "/form";
                // history.push("/form")
            })
            .catch(error => {
                console.log(error.response.data.errors);
                setError('Fields are already beem taken')
                // toast.error('Please fill form correctly', {autoClose: 3000})
                // history.push("/")
                if (error.response.data.errors) {
                    setError(error.response.data.errors);
                    // toast.success(error.response.data.errors.email, {autoClose: 3000})
                   //  alert(error.response.data.errors.email);
                   // alert(error.response.data.errors.cnic);

                }
            })

    }
    console.log(error);

        return (
        <div className="home img-fluid showcase">
            <div className="container">
                <div className="box-showcase m-auto">
                    <div className='row pt-5'>
                        <div
                            className="col-sm-12 flex-column logo-home dispaly-center">
                            <img className="home-img" src={Img} alt="n0-img"/>
                            <h1 className="logo-text theme-font">Ride shide</h1>

                        </div>
                    </div>
    <div className="row  pt-5 text-center ">
        <div
            className=" col-lg-10 form-home m-auto">
            <h1 className="heading-form">Please fill the form</h1>
                        <h1 className="heading-para">To register as a auto Driver(Ambassador)</h1>

                        <form onSubmit={handleSubmit(onSubmit)} className="p-4">
                            <div className="row">

                                <div className="col-md-12 mt-4">
                                    <Form.Field>
                                        <div className="ui left icon w-100 input">
                                            <input className="shadow-sm form-control w-100" type="text"
                                                                               maxLength="25"
                                                   ref={register({ required: true })}
                                                   placeholder="Enter Your Name" name="name"/>
                                            <i className="user icon"></i>
                                        </div>
                                    </Form.Field>
                                    <div className='text-left'>
                                        {errors.name && errors.name.type === "maxLength" && <span>Max length exceeded</span> }
                                        {errors.name && errors.name.type === "required" &&
                                        <span style={{color: "red"}}>Name is required</span>}
                                        {error.name && <span className="Errors">
                              {error.name[0]}
                                </span>}
                                    </div>
                                </div>
                                <div className="col-md-12 mt-4">
                                    <Form.Field>
                                        <div className="ui left icon w-100 input">
                                            <input className="shadow-sm form-control w-100" type="text" maxLength="11"
                                                   ref={register({required: true})}
                                                   placeholder="Enter Your Phone No" name="phone"/>
                                            <i className="phone icon"></i>
                                        </div>
                                    </Form.Field>
                                    <div className='text-left'>
                                        {errors.phone && errors.phone.type === "required" &&
                                        <span style={{color: "red"}}>Phone is required</span>}
                                        {error.phone && <span className="Errors">
                          {error.phone[0]}
                            </span>}
                                    </div>
                                </div>
                                <div className="col-md-12 mt-4">
                                    <Form.Field>
                                        <div className="ui w-100 left icon input">
                                            <input className="shadow-sm  form-control w-100" type="email"
                                                   ref={register({required: true})}
                                                   placeholder="Enter Your Email" name="email"/>
                                            <i className="mail icon"></i>
                                        </div>
                                    </Form.Field>
                                    <div className='text-left'>
                                        {errors.email && errors.email.type === "required" &&
                                        <span style={{color: "red"}}>Email is required</span>}
                                        {error.email && <span className="Errors">
                          {error.email[0]}
                            </span>}
                                    </div>
                                </div>
                                <div className="col-md-12 mt-4">
                                    <Form.Field>
                                        <div className="ui w-100 left icon input">
                                            <input className="form-control shadow w-100" maxLength="15" minLength="15" type="cnic"  data-inputmask="'mask': '99999-9999999-9'"  placeholder="CNIC N0 : XXXXX-XXXXXXX-X"
                                                   ref={register({required: true})}
                                                    name="cnic" required=""/>
                                            <i className="id card icon"></i>
                                        </div>
                                    </Form.Field>
                                    <div className='text-left'>
                                        {errors.cnic && errors.cnic.type === "required" &&
                                        <span  role="alert" style={{color: "red"}}>CNIC is required</span>}
                                        {error.cnic && <span className="Errors">
                          {error.cnic[0]}
                            </span>}
                                    </div>
                                </div>
                                <div className="col-md-12 mt-4 mb-5">
                                    <button className='btn btn-dark adjust-btn' type='submit'>Submit
                                    </button>
                                </div>
                            </div>
                        </form>
        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
export default Home;
