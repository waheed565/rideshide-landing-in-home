import React, {useEffect, useState} from 'react';
import {Input, Select} from "@chakra-ui/react";
import './style.css'
import {useForm} from "react-hook-form";
import {toast} from "react-toastify";
import axios from "axios";
import {URI} from "../../constant";

toast.configure()

const CarForm = () => {
    const [color, setColor] = useState([]);
    const {register, handleSubmit, errors} = useForm();
    const [error, setError] = useState({})

    useEffect(() => {
        const result = axios.get(URI + "getColor")
            .then(response => {
                console.log("ses", response.data.getColor);
                setColor(response.data.getColor);
            }).catch(error => {
                console.log(error);
            })
    }, [])

    const onSubmit = data => {
        axios.post(URI + 'storeCarInfo', {
            model: data.model,
            type: data.type,
            engine_capacity: data.engine_capacity,
            color_id: data.color_id,

        })
            .then(response => {
                console.log(response);
                toast.success('Data submitted successfully', {autoClose: 3000})
                window.location.href = "/";
            })
            .catch(error => {
                console.log(error);
                toast.error('Please fill form correctly', {autoClose: 3000})
                if (error.response.data.errors) {
                    setError(error.response.data.errors);
                }
            })
    }

    return (
        <div>
            <h1 className="form-heading mb-3">Please Enter Your Car Information</h1>
            <form onSubmit={handleSubmit(onSubmit)} className="p-4">
                <div className="container">
                    <div className="row p-2">
                        <div className="col-md-12">
                            <Input ref={register({required: true})} name="model"
                                   className="input-feild mb-2 shadow-sm" focusBorderColor="gray.400" type="tel"
                                   size="lg" variant="outline" placeholder="Enter Car Model"/>
                            <div className='text-left'>
                                {errors.model && errors.model.type === "required" &&
                                <span style={{color: "red"}}>Car Model is required</span>}
                                {error.model && <span className="Errors">
                                            {error.model[0]}
                                        </span>}
                            </div>
                        </div>
                        <div className="col-md-12">
                            <Select ref={register({required: true})} name="color_id"
                                    className="input-feilds shadow-sm" focusBorderColor="gray.400"
                                    placeholder="Pick Car Color">
                                {color.map(fbb =>
                                    <option key={fbb.key} value={fbb.id}>{fbb.name}</option>
                                )};
                            </Select>
                            <div className='text-left'>
                                {errors.color_id && errors.color_id.type === "required" &&
                                <span style={{color: "red"}}>Car color is required</span>}
                                {error.color_id && <span className="Errors">
                                 {error.color_id[0]}
                        </span>}
                            </div>
                        </div>
                    </div>
                    <div className="row p-2">
                        <div className="col-md-12">
                            <Input ref={register({required: true})} name="type"
                                   className="input-feild mb-2 shadow-sm" focusBorderColor="gray.400" type="tel"
                                   size="lg" variant="outline" placeholder="Enter Car Type"/>
                            <div className='text-left'>
                                {errors.type && errors.type.type === "required" &&
                                <span style={{color: "red"}}>CarType is required</span>}
                                {error.type && <span className="Errors">
                                            {error.type[0]}
                        </span>}
                            </div>
                        </div>
                        <div className="col-md-12">
                            <Input ref={register({required: true})} name="engine_capacity"
                                   className="input-feild shadow-sm"
                                   focusBorderColor="gray.400" type="tel" size="lg" variant="outline"
                                   placeholder="Car Engine Capacity"/>
                            <div className='text-left'>
                                {errors.engine_capacity && errors.engine_capacity.type === "required" &&
                                <span style={{color: "red"}}>Engin Capacity is required</span>}
                                {error.engine_capacity && <span className="Errors">
                      {error.engine_capacity[0]}
                        </span>}
                            </div>
                        </div>
                    </div>
                </div>
                <div className='row d-flex justify-content-center align-content-center   my-4'>
                    <button className='btn btn-dark adjust-btn' type='submit'>Submit
                    </button>
                </div>
            </form>
        </div>
    )
}
export default CarForm;
