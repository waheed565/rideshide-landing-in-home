import React, {useEffect} from "react";
import './App.css';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import Home from "./Component/Home";
import 'react-toastify/dist/ReactToastify.css';
import {ChakraProvider} from "@chakra-ui/react"
import {Route, Switch,} from "react-router-dom";
import Form from './Component/Form/index';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'react-toastify/dist/ReactToastify.css';
import {FormProvider , useForm} from "react-hook-form";
// import {FormStepper} from "./Stepper";
function App() {
    const methods = useForm({mode: "onBlur"});
    const {watch, errors} = methods;

    useEffect(() => {
        console.log("FORM CONTEXT", watch(), errors);
    }, [watch, errors]);

    return (
        <div className="App">
            <ChakraProvider>
                {/*<FormContext {...methods}>*/}
                {/*    <FormStepper />*/}
                {/*</FormContext> */}

                <Switch>
                    <Route exact path="/" component={Home}/>
                    <Route path="/form" component={Form}/>
                </Switch>
                {/*<Form/>*/}
            </ChakraProvider>
        </div>
    );
}

export default App;
