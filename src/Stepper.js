import React, {useState} from "react";
import {useFormContext} from "react-hook-form";
import _ from "lodash";
import Stepper from "@material-ui/core/Stepper";
import Step from "@material-ui/core/Step";
import StepLabel from "@material-ui/core/StepLabel";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import {FormOne, TwoForms} from "./Component/Forms";
import axios from "axios";
import {URI} from "./constant/index";
import {toast} from "react-toastify";
// const [error, setError] = useState({})

function getSteps() {
    return ["One", "Two"];
}


function getStepContent(step, formContent) {
    switch (step) {
        case 0:
            return <FormOne {...{formContent}} />;
        case 1:
            return <TwoForms {...{formContent}} />;
        default:
            return "Unknown step";
    }
}

export const FormStepper = () => {
    const {watch, errors} = useFormContext();
    const [activeStep, setActiveStep] = React.useState(0);
    const [compiledForm, setCompiledForm] = React.useState({});
    const steps = getSteps();
    const form = watch();

    const handleNext = () => {
        let canContinue = true;

        switch (activeStep) {
            case 0:
                setCompiledForm({...compiledForm, one: form});
                canContinue = true;
                break;
            case 1:
                setCompiledForm({...compiledForm, three: form});
                canContinue = handleSubmit({...compiledForm, three: form});
                break;
            default:
                return "not a valid step";
        }
        if (canContinue) {
            setActiveStep(prevActiveStep => prevActiveStep + 1);
        }
    };

    const handleBack = () => {
        if (activeStep > 0) {
            setActiveStep(prevActiveStep => prevActiveStep - 1);
            switch (activeStep) {
                case 1:
                    setCompiledForm({...compiledForm, three: form});
                    break;
                default:
                    return "not a valid step";
            }
        }
    };

    const handleReset = () => {
        setActiveStep(0);
        setCompiledForm({});
    };

    const handleSubmit = form => {
        if (_.isEmpty(errors)) {
            console.log("submit", form);
        }
        axios.post(URI + 'storeDriverInfo', {
            name: form.one.name,
            email: form.one.email,
            phone: form.one.phone,
            cnic: form.one.cnic,
            model: form.three.model,
            type: form.three.type,
            engine_capacity: form.three.engine_capacity,
            color_id: form.three.color_id,
        })
            .then(response => {
                console.log(response);
                toast.success('Data submitted successfully', {autoClose: 3000})
                window.location.href = "/";
            })
            .catch(error => {
                console.log(error);
                toast.error('Please fill form correctly', {autoClose: 3000})
                if (error.response.data.errors) {
                    // setError(error.response.data.errors);
                }
            })
    };

    return (
        <div>
            <Stepper activeStep={activeStep}>
                {steps.map((label, index) => {
                    const stepProps = {};
                    const labelProps = {};
                    return (
                        <Step key={label} {...stepProps}>
                            <StepLabel {...labelProps}>{label}</StepLabel>
                        </Step>
                    );
                })}
            </Stepper>
            <div>
                {activeStep === steps.length ? (
                    <div>
                        <>
                            <Typography>Completed</Typography>
                            <Button onClick={handleReset}>Close</Button>
                        </>
                    </div>
                ) : (
                    <div>
                        {getStepContent(activeStep, compiledForm)}
                        <div>
                            <Button onClick={handleBack}>Back</Button>
                            <Button variant="contained" color="primary" onClick={handleNext}>
                                {activeStep === steps.length - 1 ? "Finish" : "Next"}
                            </Button>
                        </div>
                    </div>
                )}
            </div>
        </div>
    );
};
